package com.sambarboza.restaurantmenu.data.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sambarboza.restaurantmenu.BuildConfig;
import com.sambarboza.restaurantmenu.application.Environment;
import com.sambarboza.restaurantmenu.application.RestaurantApplication;
import com.sambarboza.restaurantmenu.data.event.LoadCategoriesError;
import com.sambarboza.restaurantmenu.data.event.LoadCategoriesEvent;
import com.sambarboza.restaurantmenu.data.event.LoadCategoriesResponse;
import com.sambarboza.restaurantmenu.data.event.LoadCategoryDetails;
import com.sambarboza.restaurantmenu.data.event.LoadCategoryDetailsError;
import com.sambarboza.restaurantmenu.data.event.LoadCategoryDetailsResponse;
import com.sambarboza.restaurantmenu.data.type.Category;
import com.squareup.okhttp.OkHttpClient;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmQuery;
import retrofit.Endpoint;
import retrofit.Endpoints;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

@EBean(scope = EBean.Scope.Singleton)
public class ApiModule<T> {

    private RestaurantService mApi;
    @Bean
    Environment environment;
    @App
    RestaurantApplication application;
    EventBus mBus;
    Realm mRealm;
    ConnectivityManager mCm;

    @AfterInject
    void afterInject() {
        setUp();
        mBus = EventBus.getDefault();
        mCm = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private void setUp() {
        Endpoint endpoint = Endpoints.newFixedEndpoint(environment.getApiHost());
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(15l, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(10l, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Client client = new OkClient(okHttpClient);
        RestAdapter adapter = new RestAdapter.Builder()
                .setClient(client)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setEndpoint(endpoint)
                .setConverter(new GsonConverter(gson))
                .build();

        mApi = adapter.create(RestaurantService.class);
    }

    /**
     * Checks Internet Connectivity
     *
     * @return whether device has internet connection.
     */
    protected boolean isOnline() {
        NetworkInfo netInfo = mCm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    Realm getRealm() {
        if (mRealm == null) mRealm = Realm.getInstance(application);
        return mRealm;
    }

    /**
     * Cache loaded objects from API into Realm
     */
    protected void cacheIntoRealm(Category loadedObject) {
        getRealm().beginTransaction();
        getRealm().copyToRealmOrUpdate(loadedObject);
        getRealm().commitTransaction();
    }

    protected void cacheIntoRealm(List<Category> loadedObjects) {
        getRealm().beginTransaction();
        getRealm().copyToRealmOrUpdate(loadedObjects);
        getRealm().commitTransaction();
    }

    /**
     * Load Categories Event Listener
     */
    public void onEventBackgroundThread(LoadCategoriesEvent event) {
        try {
            List<Category> categories;
            if (isOnline()) {
                categories = mApi.getCategories();
                cacheIntoRealm(categories);
            } else {
                RealmQuery<Category> query = getRealm().where(Category.class);
                categories = query.findAll();
            }
            mBus.post(new LoadCategoriesResponse(event, categories));
        } catch (Throwable e) {
            mBus.post(new LoadCategoriesError(event, e));
        }
    }

    /**
     * Load Category Details Event Listener
     */
    public void onEventBackgroundThread(LoadCategoryDetails event) {
        try {
            Category category;
            if (isOnline()) {
                category = mApi.getCategoryDetails(event.getCategoryId());
            } else {
                RealmQuery<Category> query = getRealm().where(Category.class)
                        .equalTo("id", event.getCategoryId());
                category = query.findFirst();
            }
            cacheIntoRealm(category);
            mBus.post(new LoadCategoryDetailsResponse(event, category));
        } catch (Throwable e) {
            mBus.post(new LoadCategoryDetailsError(event, e));
        }
    }
}
