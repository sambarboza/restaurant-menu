package com.sambarboza.restaurantmenu.ui.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.data.type.Item;
import com.sambarboza.restaurantmenu.data.type.Subcategory;
import com.sambarboza.restaurantmenu.ui.adapter.ItemAdapter;
import com.sambarboza.restaurantmenu.ui.adapter.RecyclerViewItemClickListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.subcategory_itemview)
public class SubcategoryItemView extends LinearLayout {

    @ViewById
    View nameContainer;
    @ViewById
    TextView name;
    @ViewById
    RecyclerView itemsRecycler;

    public SubcategoryItemView(Context context) {
        super(context);
    }

    public SubcategoryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SubcategoryItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @AfterViews
    void afterViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        itemsRecycler.setHasFixedSize(true);
        itemsRecycler.setLayoutManager(linearLayoutManager);
    }

    public void bind(Subcategory subcategory, RecyclerViewItemClickListener<Item> itemClickListener) {
        name.setText(subcategory.getName());
        itemsRecycler.setAdapter(new ItemAdapter(subcategory.getItems(), itemClickListener));
    }
}
