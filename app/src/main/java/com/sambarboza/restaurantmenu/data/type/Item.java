package com.sambarboza.restaurantmenu.data.type;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Item extends RealmObject {

    @PrimaryKey
    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String description;
    @SerializedName("img_path")
    @Expose
    private String imgPath;
    @SerializedName("left_img_path")
    @Expose
    private String leftImgPath;
    @SerializedName("right_img_path")
    @Expose
    private String rightImgPath;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @Expose
    private String enabled;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The imgPath
     */
    public String getImgPath() {
        return imgPath;
    }

    /**
     * @param imgPath The img_path
     */
    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    /**
     * @return The leftImgPath
     */
    public String getLeftImgPath() {
        return leftImgPath;
    }

    /**
     * @param leftImgPath The left_img_path
     */
    public void setLeftImgPath(String leftImgPath) {
        this.leftImgPath = leftImgPath;
    }

    /**
     * @return The rightImgPath
     */
    public String getRightImgPath() {
        return rightImgPath;
    }

    /**
     * @param rightImgPath The right_img_path
     */
    public void setRightImgPath(String rightImgPath) {
        this.rightImgPath = rightImgPath;
    }

    /**
     * @return The subcategoryId
     */
    public String getSubcategoryId() {
        return subcategoryId;
    }

    /**
     * @param subcategoryId The subcategory_id
     */
    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    /**
     * @return The enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}