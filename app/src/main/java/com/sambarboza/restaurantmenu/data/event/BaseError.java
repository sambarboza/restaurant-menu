package com.sambarboza.restaurantmenu.data.event;

/**
 * Base Class for all requests errors that will be sent through EventBus.
 */
public class BaseError extends BaseResponse {
    private final Throwable error;

    public BaseError(BaseRequest request, Throwable error) {
        super(request);
        this.request = request;
        this.error = error;
    }

    public BaseRequest getRequest() {
        return request;
    }

    public Throwable getError() {
        return error;
    }
}
