package com.sambarboza.restaurantmenu.data.event;

public class LoadCategoryDetailsError extends BaseError {
    public LoadCategoryDetailsError(BaseRequest request, Throwable error) {
        super(request, error);
    }
}
