package com.sambarboza.restaurantmenu.data.event;

import com.sambarboza.restaurantmenu.data.type.Category;

public class LoadCategoryDetailsResponse extends BaseResponse {
    private final Category mCategory;

    public LoadCategoryDetailsResponse(BaseRequest request, Category category) {
        super(request);
        this.mCategory = category;
    }

    public Category getCategory() {
        return mCategory;
    }
}
