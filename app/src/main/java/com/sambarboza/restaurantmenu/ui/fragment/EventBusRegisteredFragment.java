package com.sambarboza.restaurantmenu.ui.fragment;

import android.app.Activity;

import de.greenrobot.event.EventBus;

public class EventBusRegisteredFragment extends BaseFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}
