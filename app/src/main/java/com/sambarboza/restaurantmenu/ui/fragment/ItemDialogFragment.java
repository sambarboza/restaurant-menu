package com.sambarboza.restaurantmenu.ui.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.application.Environment;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.item_detail)
public class ItemDialogFragment extends DialogFragment {

    @FragmentArg
    String itemTitle;
    @FragmentArg
    String itemDescription;
    @FragmentArg
    String imgPath;

    @ViewById(R.id.title)
    TextView title;
    @ViewById
    TextView description;
    @ViewById
    ImageView image;

    @Bean
    Environment environment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.item_detail, container);
    }

    /**
     * Hides the retry button if the request is not retriable.
     */
    @AfterViews
    void afterViews() {
        title.setText(itemTitle);
        description.setText(itemDescription);
        Picasso.with(image.getContext()).load(environment.getApiHost() + "/" + imgPath).into(image);
    }

}