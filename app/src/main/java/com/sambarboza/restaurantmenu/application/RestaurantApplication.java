package com.sambarboza.restaurantmenu.application;

import android.app.Application;

import com.sambarboza.restaurantmenu.data.api.ApiModule;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

import de.greenrobot.event.EventBus;

@EApplication
public class RestaurantApplication extends Application {

    @Bean
    ApiModule mApiModule;

    @Override
    public void onCreate() {
        super.onCreate();
        registerListeners();
    }

    /**
     * Register Main Listeners with EventBus
     */
    protected void registerListeners() {
        EventBus.getDefault().register(mApiModule);
    }
}
