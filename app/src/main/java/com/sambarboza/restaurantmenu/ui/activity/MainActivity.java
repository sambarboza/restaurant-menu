package com.sambarboza.restaurantmenu.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.data.event.BaseRequest;
import com.sambarboza.restaurantmenu.data.event.BaseResponse;
import com.sambarboza.restaurantmenu.ui.fragment.CategoriesFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;
    @ViewById(R.id.root)
    FrameLayout mRootView;
    private boolean mDisplayHomeAsUpEnabled = false;
    Map<BaseRequest, View> mLoadingViewsForRequests = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        getFragmentManager().beginTransaction()
                .add(R.id.root, CategoriesFragment_.builder().build()).commit();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    public boolean isDisplayHomeAsUpEnabled() {
        return mDisplayHomeAsUpEnabled;
    }

    public void setActionBarTitle(int resId, boolean displayHomeAsUp) {
        mDisplayHomeAsUpEnabled = displayHomeAsUp;
        getSupportActionBar().setTitle(resId);
        getSupportActionBar().setDisplayHomeAsUpEnabled(displayHomeAsUp);
    }

    public void setActionBarTitle(String title, boolean displayHomeAsUp) {
        mDisplayHomeAsUpEnabled = displayHomeAsUp;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(displayHomeAsUp);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * Listen for {@link BaseRequest} events and shows a loading if request has loading.
     */
    public void onEventMainThread(BaseRequest request) {
        if (request.hasLoading) {
            View loading = getLayoutInflater().inflate(R.layout.progress_default, mRootView, false);
            mLoadingViewsForRequests.put(request, loading);
            mRootView.addView(loading);
        }
    }

    /**
     * Listen for {@link BaseResponse} events and dismiss loading that
     * was shown for request of this Response
     */
    public void onEventMainThread(BaseResponse response) {
        if (mLoadingViewsForRequests.containsKey(response.getRequest())) {
            mRootView.removeView(mLoadingViewsForRequests.remove(response.getRequest()));
        }
    }

    public Map<BaseRequest, View> getLoadingViewsForRequests() {
        return mLoadingViewsForRequests;
    }

}