package com.sambarboza.restaurantmenu.data.event;

public class LoadCategoryDetails extends BaseRequest {
    private final String mCategoryId;

    public LoadCategoryDetails(String categoryId) {
        this.mCategoryId = categoryId;
    }

    public String getCategoryId() {
        return mCategoryId;
    }
}
