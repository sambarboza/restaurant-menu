package com.sambarboza.restaurantmenu.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.application.Environment;
import com.sambarboza.restaurantmenu.data.type.Item;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_itemview)
public class ItemItemView extends RelativeLayout {

    @Bean
    Environment environment;
    @ViewById
    TextView name;
    @ViewById
    ImageView image;

    public ItemItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ItemItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemItemView(Context context) {
        super(context);
    }

    public void bind(Item item) {
        name.setText(item.getName());
        String imgPath = environment.getApiHost() + "/" + item.getImgPath();
        Picasso.with(getContext()).load(imgPath).into(image);
    }

}
