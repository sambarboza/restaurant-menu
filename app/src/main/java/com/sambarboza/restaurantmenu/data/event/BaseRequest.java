package com.sambarboza.restaurantmenu.data.event;

/**
 * Base Class for all requests that will be sent through EventBus.
 */
public abstract class BaseRequest {
    public BaseRequest() {
    }

    public boolean hasLoading = true;
}
