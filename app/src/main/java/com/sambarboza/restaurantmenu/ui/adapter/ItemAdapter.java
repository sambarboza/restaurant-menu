package com.sambarboza.restaurantmenu.ui.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.sambarboza.restaurantmenu.data.type.Item;
import com.sambarboza.restaurantmenu.ui.view.ItemItemView;
import com.sambarboza.restaurantmenu.ui.view.ItemItemView_;
import com.sambarboza.restaurantmenu.ui.view.ViewWrapper;

import java.util.List;

public class ItemAdapter extends RecyclerViewAdapterBase<Item, ItemItemView> {
    public ItemAdapter(List<Item> items, RecyclerViewItemClickListener<Item> itemClickListener) {
        super(items, itemClickListener);
    }

    @Override
    protected ItemItemView onCreateItemView(ViewGroup parent, int viewType) {
        return ItemItemView_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ItemItemView> holder, int position) {
        final Item item = mItems.get(position);
        holder.getView().bind(item);
        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(item);
            }
        });
    }
}
