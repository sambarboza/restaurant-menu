package com.sambarboza.restaurantmenu.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.data.event.LoadCategoryDetails;
import com.sambarboza.restaurantmenu.data.event.LoadCategoryDetailsResponse;
import com.sambarboza.restaurantmenu.data.type.Category;
import com.sambarboza.restaurantmenu.data.type.Item;
import com.sambarboza.restaurantmenu.ui.activity.MainActivity;
import com.sambarboza.restaurantmenu.ui.adapter.RecyclerViewItemClickListener;
import com.sambarboza.restaurantmenu.ui.adapter.SubcategoryAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_category_detail)
public class CategoryDetailFragment extends EventBusRegisteredFragment implements RecyclerViewItemClickListener<Item> {

    @FragmentArg
    String categoryName;
    @FragmentArg
    String categoryId;
    @FragmentArg
    String categoryImageUrl;
    @ViewById
    RecyclerView subcategoriesRecycler;
    LoadCategoryDetailsResponse mContent;

    @AfterViews
    void afterViews() {
        ((MainActivity) getActivity()).setActionBarTitle(categoryName, true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        subcategoriesRecycler.setLayoutManager(linearLayoutManager);

        if (mContent == null) {
            EventBus.getDefault().post(new LoadCategoryDetails(categoryId));
        } else {
            onEventMainThread(mContent);
        }
    }

    /**
     * Category Detail Loaded Listener
     */
    public void onEventMainThread(LoadCategoryDetailsResponse response) {
        mContent = response;
        Category category = response.getCategory();
        subcategoriesRecycler.setAdapter(new SubcategoryAdapter(category.getSubcategory(), this));
    }

    @Override
    public void onItemClicked(Item item) {
        ItemDialogFragment_.builder()
                .itemTitle(item.getName())
                .itemDescription(item.getDescription())
                .imgPath(item.getImgPath())
                .build()
                .show(getFragmentManager(), null);
    }


}
