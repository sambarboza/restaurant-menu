package com.sambarboza.restaurantmenu.data.event;

/**
 * Base Class for all responses that will be sent through EventBus.
 */
public abstract class BaseResponse {
    BaseRequest request;

    protected BaseResponse(BaseRequest request) {
        this.request = request;
    }

    public BaseRequest getRequest() {
        return request;
    }
}