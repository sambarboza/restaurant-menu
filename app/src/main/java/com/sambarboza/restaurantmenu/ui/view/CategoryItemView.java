package com.sambarboza.restaurantmenu.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.application.Environment;
import com.sambarboza.restaurantmenu.data.type.Category;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.category_itemview)
public class CategoryItemView extends RelativeLayout {

    @ViewById
    ImageView image;
    @ViewById
    TextView name;
    @Bean
    Environment environment;

    public CategoryItemView(Context context) {
        super(context);
    }

    public CategoryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CategoryItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void bind(Category category) {
        name.setText(category.getName());
        Picasso.with(getContext()).load(environment.getApiHost() + "/" + category.getImgPath())
                .into(image);
    }
}
