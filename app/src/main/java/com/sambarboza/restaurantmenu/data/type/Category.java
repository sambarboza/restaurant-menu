package com.sambarboza.restaurantmenu.data.type;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Category extends RealmObject {

    @PrimaryKey
    @Expose
    private String id;
    @Expose
    private String name;
    @SerializedName("img_path")
    @Expose
    private String imgPath;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @Expose
    private String enabled;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @Expose
    private RealmList<Subcategory> subcategory = new RealmList<>();

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The imgPath
     */
    public String getImgPath() {
        return imgPath;
    }

    /**
     * @param imgPath The img_path
     */
    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    /**
     * @return The typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId The type_id
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return The enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The subcategory
     */
    public RealmList<Subcategory> getSubcategory() {
        return subcategory;
    }

    /**
     * @param subcategory The subcategory
     */
    public void setSubcategory(RealmList<Subcategory> subcategory) {
        this.subcategory = subcategory;
    }

}