package com.sambarboza.restaurantmenu.ui.adapter;

import android.view.ViewGroup;

import com.sambarboza.restaurantmenu.data.type.Item;
import com.sambarboza.restaurantmenu.data.type.Subcategory;
import com.sambarboza.restaurantmenu.ui.view.SubcategoryItemView;
import com.sambarboza.restaurantmenu.ui.view.SubcategoryItemView_;
import com.sambarboza.restaurantmenu.ui.view.ViewWrapper;

import java.util.List;

public class SubcategoryAdapter extends RecyclerViewAdapterBase<Subcategory, SubcategoryItemView> {

    RecyclerViewItemClickListener<Item> itemClickListener;

    public SubcategoryAdapter(List<Subcategory> items, RecyclerViewItemClickListener<Item> itemClickListener) {
        super(items, null);
        this.itemClickListener = itemClickListener;
    }

    @Override
    protected SubcategoryItemView onCreateItemView(ViewGroup parent, int viewType) {
        return SubcategoryItemView_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<SubcategoryItemView> holder, int position) {
        final Subcategory subcategory = mItems.get(position);
        holder.getView().bind(subcategory, itemClickListener);
    }
}
