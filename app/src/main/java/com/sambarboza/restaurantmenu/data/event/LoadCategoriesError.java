package com.sambarboza.restaurantmenu.data.event;

public class LoadCategoriesError extends BaseError {
    public LoadCategoriesError(BaseRequest request, Throwable error) {
        super(request, error);
    }
}
