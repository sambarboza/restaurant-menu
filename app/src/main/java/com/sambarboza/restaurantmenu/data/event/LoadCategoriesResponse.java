package com.sambarboza.restaurantmenu.data.event;

import com.sambarboza.restaurantmenu.data.type.Category;

import java.util.List;

public class LoadCategoriesResponse extends BaseResponse {
    private final List<Category> mCategories;

    public LoadCategoriesResponse(BaseRequest request, List<Category> categories) {
        super(request);
        mCategories = categories;
    }

    public List<Category> getCategories() {
        return mCategories;
    }
}
