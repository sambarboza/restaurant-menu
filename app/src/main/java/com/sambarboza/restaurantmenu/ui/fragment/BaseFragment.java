package com.sambarboza.restaurantmenu.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.sambarboza.restaurantmenu.ui.activity.MainActivity;

public abstract class BaseFragment extends Fragment {

    boolean mSetDisplayHomeAsUpOnDetach;
    String mPreviousTitle;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSetDisplayHomeAsUpOnDetach = ((MainActivity) getActivity()).isDisplayHomeAsUpEnabled();
        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            mPreviousTitle = ((MainActivity) getActivity()).getSupportActionBar().getTitle().toString();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((MainActivity) getActivity()).setActionBarTitle(mPreviousTitle, mSetDisplayHomeAsUpOnDetach);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

}
