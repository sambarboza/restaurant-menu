package com.sambarboza.restaurantmenu.data.api;

import com.sambarboza.restaurantmenu.data.type.Category;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface RestaurantService {

    @GET("/category")
    List<Category> getCategories();

    @GET("/category/details/{id}")
    Category getCategoryDetails(@Path("id") String id);

}
