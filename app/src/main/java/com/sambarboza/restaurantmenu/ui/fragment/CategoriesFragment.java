package com.sambarboza.restaurantmenu.ui.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sambarboza.restaurantmenu.R;
import com.sambarboza.restaurantmenu.application.Environment;
import com.sambarboza.restaurantmenu.data.event.LoadCategoriesEvent;
import com.sambarboza.restaurantmenu.data.event.LoadCategoriesResponse;
import com.sambarboza.restaurantmenu.data.type.Category;
import com.sambarboza.restaurantmenu.ui.adapter.CategoryAdapter;
import com.sambarboza.restaurantmenu.ui.adapter.RecyclerViewItemClickListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_categories)
public class CategoriesFragment extends EventBusRegisteredFragment implements RecyclerViewItemClickListener<Category> {

    @Bean
    Environment environment;
    @ViewById(R.id.categories_recycler)
    RecyclerView mCategoriesRecycler;
    List<Category> mCategories;

    @AfterViews
    void afterViews() {
        if (environment.isTablet()) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            gridLayoutManager.setOrientation(RecyclerView.VERTICAL);
            mCategoriesRecycler.setLayoutManager(gridLayoutManager);
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mCategoriesRecycler.setLayoutManager(linearLayoutManager);
        }
        mCategoriesRecycler.setHasFixedSize(true);
        if (mCategories == null) {
            EventBus.getDefault().post(new LoadCategoriesEvent());
        } else {
            mCategoriesRecycler.setAdapter(new CategoryAdapter(mCategories, this));
        }
    }

    /**
     * Listener for {@link LoadCategoriesResponse}
     */
    public void onEventMainThread(LoadCategoriesResponse response) {
        mCategories = response.getCategories();
        mCategoriesRecycler.setAdapter(new CategoryAdapter(mCategories, this));
    }

    @Override
    public void onItemClicked(Category category) {
        CategoryDetailFragment fragment = CategoryDetailFragment_.builder()
                .categoryId(category.getId())
                .categoryName(category.getName())
                .categoryImageUrl(environment.getApiHost() + "/" + category.getImgPath())
                .build();
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fragment_in, R.anim.fragment_out,
                        R.anim.fragment_in, R.anim.fragment_out).addToBackStack(null)
                .add(R.id.root, fragment)
                .commit();
    }
}
