package com.sambarboza.restaurantmenu.ui.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.sambarboza.restaurantmenu.data.type.Category;
import com.sambarboza.restaurantmenu.ui.view.CategoryItemView;
import com.sambarboza.restaurantmenu.ui.view.CategoryItemView_;
import com.sambarboza.restaurantmenu.ui.view.ViewWrapper;

import java.util.List;

public class CategoryAdapter extends RecyclerViewAdapterBase<Category, CategoryItemView> {

    public CategoryAdapter(List<Category> items, RecyclerViewItemClickListener<Category> itemClickListener) {
        super(items, itemClickListener);
    }

    @Override
    protected CategoryItemView onCreateItemView(ViewGroup parent, int viewType) {
        return CategoryItemView_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<CategoryItemView> holder, int position) {
        final Category category = mItems.get(position);
        holder.getView().bind(category);
        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(category);
            }
        });
    }
}
