package com.sambarboza.restaurantmenu.application;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.BooleanRes;
import org.androidannotations.annotations.res.StringRes;

@EBean(scope = EBean.Scope.Singleton)
public class Environment {
    @StringRes
    String apiHost;
    @BooleanRes
    boolean isTablet;

    public String getApiHost() {
        return apiHost;
    }

    public boolean isTablet() {
        return isTablet;
    }
}
