package com.sambarboza.restaurantmenu.ui.adapter;

public interface RecyclerViewItemClickListener<T> {
    public void onItemClicked(T item);
}
